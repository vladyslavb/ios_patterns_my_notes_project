//
//  PersonPrototypeObject.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "PersonPrototypeObject.h"

@implementation PersonPrototypeObject

- (id) copyWithZone: (NSZone*) zone
{
    PersonPrototypeObject* copy = [[self class] allocWithZone: zone];
    
    copy.name    = self.name;
    copy.surname = self.surname;
    copy.age     = self.age;
    
    return copy;
}


@end
