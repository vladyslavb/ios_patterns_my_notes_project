//
//  Person.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonPrototypeObject : NSObject <NSCopying>

//properties
@property (weak, nonatomic) NSString* name;
@property (weak, nonatomic) NSString* surname;
@property (weak, nonatomic) NSString* age;

@end
