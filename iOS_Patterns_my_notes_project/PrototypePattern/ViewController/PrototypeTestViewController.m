//
//  PrototypeTestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "PrototypeTestViewController.h"

//classes

#import "PersonPrototypeObject.h"

@interface PrototypeTestViewController ()

@end

@implementation PrototypeTestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    //simple for code (wrong code)
    [self testPrototypePatternWrongCode];
    
    //simple for code (correct code)
    [self testPrototypePatternCorrectCode];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods (Presentation Test Pattertn) -

- (void) testPrototypePatternWrongCode
{
    // ---> HERE CODE BEFORE ---> ITS WRONG CODE
    
    PersonPrototypeObject* firstPerson = [[PersonPrototypeObject alloc] init];
    
    firstPerson.name    = @"Vladyslav";
    firstPerson.surname = @"Bedro";
    
    PersonPrototypeObject* secondPerson = firstPerson;
    
    NSLog(@"First Person name = %@ and surname = %@",
                                        firstPerson.name,
                                        firstPerson.surname);
    
    secondPerson.name = @"John";
    
    NSLog(@"Second Person name = %@ and surname = %@",
                                        secondPerson.name,
                                        secondPerson.surname);
    
    NSLog(@"First Person name = %@ and surname = %@",
                                        firstPerson.name,
                                        firstPerson.surname);
    
    /*
     
     LOG: ---->
     
        First Person name = Dima
     
        Second Person name = Roma
     
        First Person name = Roma
     
     */
}

- (void) testPrototypePatternCorrectCode
{
    // ---> HERE CODE AFTER ---> ITS CORRECT CODE
    
    PersonPrototypeObject* firstPerson = [[PersonPrototypeObject alloc] init];
    
    firstPerson.name    = @"Vladyslav";
    firstPerson.surname = @"Bedro";
    
    PersonPrototypeObject* secondPerson = firstPerson.copy; // (!)
    
    NSLog(@"First Person name = %@ and surname = %@",
          firstPerson.name,
          firstPerson.surname);
    
    secondPerson.name = @"John";
    
    NSLog(@"Second Person name = %@ and surname = %@",
          secondPerson.name,
          secondPerson.surname);
    
    NSLog(@"First Person name = %@ and surname = %@",
          firstPerson.name,
          firstPerson.surname);
    
    /*
     
     LOG: ---->
     
     First Person name = Dima
     
     Second Person name = Roma
     
     First Person name = Dima
     
     */
}


@end
































