//
//  EuropeanNotebookCharger.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Charger.h"

//protocols

#import "EuropeanNotebookChargerDelegate.h"

@interface EuropeanNotebookCharger : Charger <EuropeanNotebookChargerDelegate>
{
    @private id<EuropeanNotebookChargerDelegate> _delegate;
}

//properties
@property (nonatomic, strong) id<EuropeanNotebookChargerDelegate> delegate;

@end
