//
//  EuropeanNotebookCharger.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "EuropeanNotebookCharger.h"

@implementation EuropeanNotebookCharger

// for old realization code
@synthesize delegate = _delegate;


#pragma mark - Initialization -

- (id) init
{
    if ( self = [super init] )
    {
        self.delegate = self;
    }
    
    return self;
}

- (void) charge
{
    [_delegate chargetNotebookRoundHoles: self];
    
    [super charge];
}


#pragma mark - EuropeanNotebookChargerDelegate methods -

- (void) chargetNotebookRoundHoles: (Charger*) charger
{
    //Do smth with charger
    NSLog(@"Charging with 220 and round holes!"); }

@end
