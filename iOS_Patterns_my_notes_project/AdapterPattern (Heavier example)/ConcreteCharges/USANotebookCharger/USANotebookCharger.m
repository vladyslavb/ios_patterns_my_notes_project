//
//  USANotebookCharger.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "USANotebookCharger.h"

@implementation USANotebookCharger


#pragma mark - Public methods -

- (void) chargeNotebookRectHoles: (Charger*) charger
{
    NSLog(@"Charge Notebook Rect Holes");
}

@end
