//
//  AdapterPatternTest2ViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AdapterPatternTest2ViewController.h"

//classes
#import "Charger.h"
#import "USANotebookCharger.h"
#import "EuropeanNotebookCharger.h"

#import "USANotebookEuropeanAdapter.h"

@interface AdapterPatternTest2ViewController ()

@end

@implementation AdapterPatternTest2ViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self startTestChargingWithAdapter];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startTestChargingWithAdapter
{
    EuropeanNotebookCharger* euroCharger = [[EuropeanNotebookCharger alloc] init];
    
    [self makeTheNotebookCharge: euroCharger];
    
    USANotebookCharger* charger = [[USANotebookCharger alloc] init];
    
    USANotebookEuropeanAdapter* adapter =
        [[USANotebookEuropeanAdapter alloc] initWithUSANotebookCharger: charger];
    
    [self makeTheNotebookCharge: adapter];
}

- (void) makeTheNotebookCharge: (Charger*) aCharger
{
    [aCharger charge];
}

/*
 
 LOG --------------->
 
    Charging with 220 and round holes!
 
    C’mon I am charging
 
    Charge Notebook Rect Holes
 
    C’mon I am charging
 
 */

@end


































