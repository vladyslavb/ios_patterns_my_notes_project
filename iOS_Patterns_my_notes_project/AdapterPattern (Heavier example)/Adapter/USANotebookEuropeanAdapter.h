//
//  USANotebookEuropeanAdapter.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//


//classes
#import "Charger.h"
#import "USANotebookCharger.h"

//protocols
#import "EuropeanNotebookChargerDelegate.h"

//clases

@interface USANotebookEuropeanAdapter : Charger <EuropeanNotebookChargerDelegate>

//properties
@property (nonatomic, strong) USANotebookCharger* usaCharger;


//methods
- (id)   initWithUSANotebookCharger: (USANotebookCharger*) charger;

- (void) charge;

@end
