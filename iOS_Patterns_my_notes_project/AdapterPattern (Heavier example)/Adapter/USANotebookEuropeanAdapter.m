//
//  USANotebookEuropeanAdapter.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "USANotebookEuropeanAdapter.h"

//clases
#import "USANotebookCharger.h"
#import "EuropeanNotebookCharger.h"

@implementation USANotebookEuropeanAdapter


#pragma mark - Public methods -

- (id) initWithUSANotebookCharger: (USANotebookCharger*) charger
{
    if ( self = [super init] )
    {
        self.usaCharger = charger;
    }
    
    return self;
}

- (void) charge
{
    EuropeanNotebookCharger* euroCharge = [[EuropeanNotebookCharger alloc] init];
    
    euroCharge.delegate = self;
    
    [euroCharge charge];
}


#pragma mark - EuropeanNotebookChargerDelegate -

- (void) chargetNotebookRoundHoles: (Charger*) charger
{
    [self.usaCharger chargeNotebookRectHoles: charger];
}

@end
