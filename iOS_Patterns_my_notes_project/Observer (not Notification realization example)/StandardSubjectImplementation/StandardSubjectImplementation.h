//
//  StandardSubjectImplementation.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//protocols
#import "StandardSubject.h"

@interface StandardSubjectImplementation : NSObject <StandardSubject>
{
    @private NSString* _valueName;
    @private NSString* _newValue;
}

//properties
@property (strong, nonatomic) NSMutableSet* observerCollection;

//methods
- (void) changeValue: (NSString*) valueName
            andValue: (NSString*) newValue;

@end
