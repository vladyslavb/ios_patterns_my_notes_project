//
//  StandardSubjectImplementation.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "StandardSubjectImplementation.h"

//protocols
#import "StandardObserver.h"

@implementation StandardSubjectImplementation


#pragma mark - Initialization -

- (NSMutableSet*) observerCollection
{
    if (_observerCollection == nil)
    {
     _observerCollection = [[NSMutableSet alloc] init];
    }
    
    return _observerCollection;
}


#pragma mark - StandardSubject protocol methods -

- (void) addObserver: (id<StandardObserver>) observer
{
    [self.observerCollection addObject: observer];
}

- (void) removeObserver: (id<StandardObserver>) observer
{
    [self.observerCollection removeObject: observer];
}

- (void) notifyObjects
{
    for (id<StandardObserver> observer in self.observerCollection)
    {
        [observer valueChanged: _valueName
                      newValue: _newValue];
    }
}


#pragma mark - Public methods -

- (void) changeValue: (NSString*) valueName
            andValue: (NSString*) newValue
{
    _newValue  = newValue;
    _valueName = valueName;
    
    [self notifyObjects];
}

@end
