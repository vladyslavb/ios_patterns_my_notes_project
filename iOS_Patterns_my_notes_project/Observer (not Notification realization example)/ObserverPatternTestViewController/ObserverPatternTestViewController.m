//
//  ObserverPatternTestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ObserverPatternTestViewController.h"

//classes
#import "StandardSubjectImplementation.h"

#import "SomeSubscriber.h"
#import "OtherSubscriber.h"

@interface ObserverPatternTestViewController ()

@end

@implementation ObserverPatternTestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startObserverPatternTesting
{
    StandardSubjectImplementation* subj = [[StandardSubjectImplementation alloc] init];
    
    SomeSubscriber* someSubscriber = [[SomeSubscriber alloc] init];
    
    OtherSubscriber* otherSubscriber = [[OtherSubscriber alloc] init];
    
    [subj addObserver: someSubscriber];
    [subj addObserver: otherSubscriber];
    
    [subj changeValue: @"strange value"
             andValue: @"newValue"];
}

/*
 
    LOG ------------------->
 
         And some subscriber tells: Hmm, value strange value changed to newValue
         
         And some another subscriber tells: Hmm, value strange value changed to newValue
 
 */

@end





























