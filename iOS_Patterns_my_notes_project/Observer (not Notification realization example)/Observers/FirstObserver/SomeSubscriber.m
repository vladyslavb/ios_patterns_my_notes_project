//
//  SomeSubscriber.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "SomeSubscriber.h"

@implementation SomeSubscriber


#pragma mark - StandardObserver protocol methods -

- (void) valueChanged: (NSString*) valueName
             newValue: (NSString*) newValue
{
    NSLog(@"And some subscriber tells: Hmm, value %@ changed to %@",
                                                                valueName,
                                                                newValue);
}

@end
