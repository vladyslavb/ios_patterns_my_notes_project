//
//  OtherSubscriber.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "OtherSubscriber.h"

@implementation OtherSubscriber


#pragma mark - StandardObserver protocol methods -

- (void) valueChanged: (NSString*) valueName
             newValue: (NSString*) newValue
{
    NSLog(@"And some another subscriber tells: Hmm, value %@ changed to %@",
                                                                        valueName,
                                                                        newValue);
}

@end
