//
//  StandardSubject.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//protocols
#import "StandardObserver.h"

@protocol StandardSubject <NSObject>

//required methods
- (void) addObserver: (id<StandardObserver>) observer;

- (void) removeObserver: (id<StandardObserver>) observer;

- (void) notifyObjects;

@end
