//
//  VisitorPatternTestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "VisitorPatternTestViewController.h"

//classes
#import "Warehouse.h"
#import "WarehouseItem.h"
#import "PriceCheckerVisitor.h"
#import "QualityCheckerVisitor.h"

@interface VisitorPatternTestViewController ()

@end

@implementation VisitorPatternTestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startVisitorPatternTesting
{
    //Generating test data
    Warehouse* _localWarehouse = [[Warehouse alloc] init];
    
    [_localWarehouse addItem: [[WarehouseItem alloc] initWithArgs: @"Item1"
                                                       andQuality: NO
                                                         andPrice: 25]];
    
    [_localWarehouse addItem: [[WarehouseItem alloc] initWithArgs: @"Item2"
                                                       andQuality: NO
                                                         andPrice: 32]];
    
    [_localWarehouse addItem: [[WarehouseItem alloc] initWithArgs: @"Item3"
                                                       andQuality: YES
                                                         andPrice: 45]];
    
    [_localWarehouse addItem: [[WarehouseItem alloc] initWithArgs: @"Item4"
                                                       andQuality: NO
                                                         andPrice: 33]];
    
    [_localWarehouse addItem: [[WarehouseItem alloc] initWithArgs: @"Item5"
                                                       andQuality: NO
                                                         andPrice: 12]];
    
    [_localWarehouse addItem: [[WarehouseItem alloc] initWithArgs: @"Item6"
                                                       andQuality: YES
                                                         andPrice: 78]];
    
    [_localWarehouse addItem: [[WarehouseItem alloc] initWithArgs: @"Item7"
                                                       andQuality: YES
                                                         andPrice: 34]];
    
    [_localWarehouse addItem: [[WarehouseItem alloc] initWithArgs: @"Item8"
                                                       andQuality: NO
                                                         andPrice: 51]];
    
    [_localWarehouse addItem: [[WarehouseItem alloc] initWithArgs: @"Item9"
                                                       andQuality: NO
                                                         andPrice: 25]];
    
    // Testing...
    
    PriceCheckerVisitor* priceVisitor = [[PriceCheckerVisitor alloc] init];

    QualityCheckerVisitor* qualityVisitor = [[QualityCheckerVisitor alloc] init];
    
    [_localWarehouse accept: priceVisitor];
    [_localWarehouse accept: qualityVisitor];
}

/*
 
 LOG -------------------->
 
             Hmmm, I don’t know how much Warehouse costs!
 
             Item1 have price = 25
             Item2 have price = 32
             Item3 have price = 45
             Item4 have price = 33
             Item5 have price = 12
             Item6 have price = 78
             Item7 have price = 34
             Item8 have price = 51
             Item9 have price = 25
 
             Hmmm, nice warehouse!
 
             Item: Item1 is pretty cool!
             Item2 is pretty cool!
             Item3 is broken
             Item4 is pretty cool!
             Item5 is pretty cool!
             Item6 is broken
             Item7 is broken
             Item8 is pretty cool!
             Item9 is pretty cool!
 
 */




@end































