//
//  PriceCheckerVisitor.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "PriceCheckerVisitor.h"

//classes
#import "Warehouse.h"
#import "WarehouseItem.h"

@implementation PriceCheckerVisitor


#pragma mark - BasicVisitor protocol methods -

- (void) visit: (id) anObject
{
    if ([anObject isKindOfClass: [WarehouseItem class]])
    {
        NSLog(@"Item: %@ have price = %i", [anObject name], [anObject price]);
    }
    
    if ([anObject isKindOfClass: [Warehouse class]])
    {
        NSLog(@"Hmmm, I don't know how much Warehouse costs!");
        
        return;
    }
}

@end
