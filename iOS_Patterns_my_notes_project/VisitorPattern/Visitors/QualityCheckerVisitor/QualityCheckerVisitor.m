//
//  QualityCheckerVisitor.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "QualityCheckerVisitor.h"

//classes
#import "Warehouse.h"
#import "WarehouseItem.h"

@implementation QualityCheckerVisitor


#pragma mark - BasicVisitor protocol methods -

- (void) visit: (id) anObject
{
    if ([anObject isKindOfClass: [WarehouseItem class]])
    {
        if ([anObject isBroken])
        {
            NSLog(@"Item: %@ is broken", [anObject name]);
        }
        else
        {
            NSLog(@"Item: %@ is pretty cool!", [anObject name]);
        }
    }
    
    if ([anObject isKindOfClass: [Warehouse class]])
    {
        NSLog(@"Hmmm, nice warehouse!");
        
        return;
    }
}

@end
