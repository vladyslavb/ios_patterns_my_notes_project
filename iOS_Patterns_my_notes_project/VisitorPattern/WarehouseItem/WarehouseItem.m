//
//  WarehouseItem.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "WarehouseItem.h"

@implementation WarehouseItem


#pragma mark - Initialization -

- (id) initWithArgs: (NSString*) aName
         andQuality: (BOOL)      isBrokenState
           andPrice: (int)       aPrice
{
    if (self = [super init])
    {
        self.name     = aName;
        self.isBroken = isBrokenState;
        self.price    = aPrice;
    }
    
    return self;
}

@end
