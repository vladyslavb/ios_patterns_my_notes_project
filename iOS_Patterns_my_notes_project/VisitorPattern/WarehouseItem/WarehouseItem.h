//
//  WarehouseItem.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WarehouseItem : NSObject

//properties
@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) BOOL      isBroken;
@property (assign, nonatomic) int       price;

//methods
-(id) initWithArgs: (NSString*) aName
        andQuality: (BOOL)      isBrokenState
          andPrice: (int)       aPrice;


@end
