//
//  Warehouse.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Warehouse.h"

@implementation Warehouse

#pragma mark - BasicVisitor protocol methods -

- (void) addItem: (WarehouseItem*) anItem
{
    if (!_itemsArray)
    {
        _itemsArray = [[NSMutableArray alloc] init];
    }
    
    [_itemsArray addObject: anItem];
}

- (void) accept: (id<BasicVisitor>) visitor
{
    [visitor visit: self];
    
    for (WarehouseItem* item in _itemsArray)
    {
        [visitor visit: item];
    }
}

@end
