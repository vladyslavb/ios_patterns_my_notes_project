//
//  Warehouse.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//classes
@class WarehouseItem;

//protocols
#import "BasicVisitor.h"

@interface Warehouse : NSObject
{
    @private NSMutableArray* _itemsArray;
}

//methods
-(void) addItem: (WarehouseItem*)   anItem;
-(void) accept:  (id<BasicVisitor>) visitor;

@end
