//
//  AndroidMaker.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AndroidMaker.h"

@implementation AndroidMaker


#pragma mark - Overriding methods -

- (void) assemble
{
    [super assemble];
    
    [self addCPU];
    [self addRam];
}


#pragma mark - Public methods -

- (void) addCPU
{
    NSLog(@"Installing 4 more CPUs");
}

- (void) addRam
{
    NSLog(@"Installing 2Gigs of RAM");
}

@end
