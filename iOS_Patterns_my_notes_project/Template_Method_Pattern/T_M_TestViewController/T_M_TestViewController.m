//
//  T_M_TestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "T_M_TestViewController.h"

//classes
#import "iPhoneMaker.h"
#import "AndroidMaker.h"

@interface T_M_TestViewController ()

@end

@implementation T_M_TestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startTemplateMethodPatternTesting
{
    iPhoneMaker*  iphone  = [[iPhoneMaker  alloc] init];
    AndroidMaker* android = [[AndroidMaker alloc] init];
    
    [iphone  makePhone];
    [android makePhone];
}

/*
 
 LOG ------------------>
 
             Taking a box
             Taking a camera
             Taking a microphone
 
             Installing 4 more CPUs
             Installing 2Gigs of RAM
 
             Assembling everythig
 
             Putting label ‘Designed in California’
 
             Taking a box
             Taking a camera
             Taking a microphone
 
             Assembling everything
 
 */


@end



































