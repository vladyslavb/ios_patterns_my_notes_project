//
//  AnyPhoneTemplate.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AnyPhoneTemplate.h"

@implementation AnyPhoneTemplate

//our template method

#pragma mark - Public template methods -

- (void) makePhone
{
    [self takeBox];
    [self takeCamera];
    [self takeMicrophone];
    [self assemble];
}

- (void) takeBox
{
    NSLog(@"Taking a box");
}

- (void) takeCamera
{
    NSLog(@"Taking a camera");
}

- (void) takeMicrophone
{
    NSLog(@"Taking a microphone");
}

- (void) assemble
{
    NSLog(@"Assembling everythig");
}

@end

























