//
//  iPhoneMaker.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "iPhoneMaker.h"

@implementation iPhoneMaker


#pragma mark - Overriding methods -

-(void) takeBox
{
    [super takeBox];
    
    [self design];
}


#pragma mark - Public methods -

-(void) design
{
    NSLog(@"Putting label 'Designed in California'");
}

@end
