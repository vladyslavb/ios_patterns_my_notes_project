//
//  ChinaPad.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ChinaPad.h"

@implementation ChinaPad

//methods

- (id) init
{
    self = [super init];
    
    self.osName      = @"Windows CE";
    self.productName = @"Buan Que Ipado Killa";
    self.screenSize  = [[NSNumber alloc] initWithFloat: 12.5f];
    
    return self;
}

@end
