//
//  AppleIPad.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "GenericIPad.h"

@interface AppleIPad : GenericIPad

@end
