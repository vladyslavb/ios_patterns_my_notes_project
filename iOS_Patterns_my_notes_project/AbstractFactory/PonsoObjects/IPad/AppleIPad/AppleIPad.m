//
//  AppleIPad.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AppleIPad.h"

@implementation AppleIPad

//methods

- (id) init
{
    self = [super init];
    
    self.osName      = @"iOS";
    self.productName = @"IPad";
    
    self.screenSize = [[NSNumber alloc] initWithFloat: 7.7f];
    
    return self;
}

@end
