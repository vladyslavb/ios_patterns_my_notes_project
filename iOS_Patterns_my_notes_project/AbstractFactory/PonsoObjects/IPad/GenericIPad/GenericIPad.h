//
//  GenericIPad.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GenericIPad : NSObject

//properties
@property (weak,nonatomic)   NSString* osName;
@property (weak,nonatomic)   NSString* productName;
@property (strong,nonatomic) NSNumber* screenSize;

@end
