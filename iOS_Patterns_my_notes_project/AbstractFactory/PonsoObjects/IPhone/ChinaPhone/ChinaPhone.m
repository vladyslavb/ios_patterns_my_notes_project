//
//  ChinaPhone.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ChinaPhone.h"

@implementation ChinaPhone

//methods

- (id) init
{
    self = [super init];
    
    self.osName      = @"Android";
    self.productName = @"Chi Huan Hua Phone";
    
    return self;
}

@end
