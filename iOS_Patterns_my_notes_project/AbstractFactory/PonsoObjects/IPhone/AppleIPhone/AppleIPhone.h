//
//  AppleIPhone.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

//classes
#import "GenericIPhone.h"

@interface AppleIPhone : GenericIPhone

@end
