//
//  AppleIPhone.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AppleIPhone.h"

@implementation AppleIPhone

//methods

- (id) init
{
    self = [super init];
    
    self.osName      = @"iOS";
    self.productName = @"IPhone";
    
    return self;
}

@end
