//
//  AF_TestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AF_TestViewController.h"

//classes
#import "IPhoneFactory.h"
#import "AppleFactory.h"
#import "ChinaFactory.h"

#import "GenericIPad.h"
#import "GenericIPhone.h"

@interface AF_TestViewController ()

//properties
@property (assign, nonatomic) BOOL isThirdWorld;

@end

@implementation AF_TestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self createPhones];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Public methods -

- (IPhoneFactory*) getFactory
{
    if (_isThirdWorld)
    {
        return [[ChinaFactory alloc] init];
    }
    
    return [[AppleFactory alloc] init];
}


#pragma mark - Internal methods -

- (void) createPhones
{
    _isThirdWorld = false; // first was "true"_value
    
    IPhoneFactory* factory = self.getFactory;

    GenericIPad*     ipad = factory.getIPad;
    GenericIPhone* iphone = factory.getIPhone;
    
    NSLog(@"IPad named = %@, osname = %@, screensize = %@",
                                                ipad.productName,
                                                ipad.osName,
                                                ipad.screenSize.stringValue);
    
    NSLog(@"IPhone named = %@, osname = %@",
                                    iphone.productName,
                                    iphone.osName);
}

/*
 
 LOG ------->
 
 При _isThirdWorld == true будет:
 
 IPad named = Buan Que Ipado Killa, osname = Windows CE, screensize = 12.5
 
 IPhone named = Chi Huan Hua Phone, osname = Android
 
 Теперь, просто поменяв значение переменной _isThirdWorld на false, и лог будет совсем другой:
 
 IPad named = IPad, osname = iOS, screensize = 7.7
 
 IPhone named = IPhone, osname = iOS
 
 */


@end





























