//
//  AppleFactory.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AppleFactory.h"

//classes
#import "GenericIPhone.h"
#import "GenericIPad.h"

#import "AppleIPad.h"
#import "AppleIPhone.h"
#import "AppleFactory.h"

@implementation AppleFactory

//methods

- (GenericIPhone*) getIPhone
{
    AppleIPhone* iphone = [[AppleIPhone alloc] init];
    
    return iphone;
}

- (GenericIPad*) getIPad
{
    AppleIPad* ipad = [[AppleIPad alloc] init];
    
    return ipad;
}

@end
