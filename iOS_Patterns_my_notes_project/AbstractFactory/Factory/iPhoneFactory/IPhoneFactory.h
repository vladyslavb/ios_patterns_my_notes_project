//
//  IPhoneFactory.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//classes
@class GenericIPhone;
@class GenericIPad;

@interface IPhoneFactory : NSObject

// abstract methods for overriding

- (GenericIPhone*) getIPhone;
- (GenericIPad*)   getIPad;

@end
