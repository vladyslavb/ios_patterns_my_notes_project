//
//  IPhoneFactory.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/17/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "IPhoneFactory.h"

//classes

#import "GenericIPad.h"
#import "GenericIPhone.h"

@implementation IPhoneFactory

// abstract [public] methods for overriding

- (GenericIPhone*) getIPhone
{
    return [[GenericIPhone alloc] init];
}

- (GenericIPad*)   getIPad
{
    return [[GenericIPad alloc] init];
}

@end
