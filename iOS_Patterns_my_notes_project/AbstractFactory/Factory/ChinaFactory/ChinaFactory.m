//
//  ChinaFactory.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ChinaFactory.h"

//classes
#import "GenericIPhone.h"
#import "GenericIPad.h"

#import "ChinaPad.h"
#import "ChinaPhone.h"

@implementation ChinaFactory

- (GenericIPad*) getIPad
{
    ChinaPad* pad = [[ChinaPad alloc] init];
    
    return pad;
    
}

- (GenericIPhone*) getIPhone
{
    ChinaPhone* phone = [[ChinaPhone alloc] init];
    
    return phone;
}

@end
