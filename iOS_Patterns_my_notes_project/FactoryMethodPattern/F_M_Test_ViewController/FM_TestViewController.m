//
//  FM_TestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "FM_TestViewController.h"

//classes
#import "ProductGenerator.h"
#import "Product.h"

@interface FM_TestViewController ()

@end

@implementation FM_TestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    //simple for code
    [self saveExpenses: 50];
    [self saveExpenses: 56];
    [self saveExpenses: 79];
    [self saveExpenses: 100];
    [self saveExpenses: 123];
    [self saveExpenses: 51];
    
    /*
     
     LOG: ---->
     
     Saving object into Toys database
     
     Saving object into Toys database
     
     Saving object into Toys database
     
     Saving object into Dress database
     
     Saving object into Dress database
     
     Saving object into Toys database
     
     */
}


#pragma mark - Memory managment -


- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods (Presentation Test Pattertn) -

- (void) saveExpenses: (int) aPrice
{
    ProductGenerator* pd = [[ProductGenerator alloc] init];
    
    Product* expense = [pd getProduct: aPrice];
    
    [expense saveObject];
}


@end































