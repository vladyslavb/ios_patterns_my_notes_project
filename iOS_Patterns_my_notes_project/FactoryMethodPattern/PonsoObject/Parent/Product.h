//
//  Product.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

//properties
@property (strong, nonatomic) NSString*  name;
@property (assign ,nonatomic) int        price;

//methods
- (void) saveObject;

- (NSInteger) getTotalPrice: (int) sum;

@end
