//
//  Product.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Product.h"

@implementation Product


#pragma mark - Public methods -

- (void) saveObject
{
    NSLog(@"I am saving an object in to product database");
}


- (NSInteger) getTotalPrice: (int) sum
{
    return self.price + sum;
}


@end
