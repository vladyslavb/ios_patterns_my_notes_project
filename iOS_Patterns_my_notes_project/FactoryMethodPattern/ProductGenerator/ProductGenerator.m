//
//  ProductGenerator.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ProductGenerator.h"

//classes
#import "Product.h"

#import "Toy_FM.h"
#import "Dress_FM.h"

@implementation ProductGenerator


#pragma mark - Public methods -

- (Product*) getProduct: (int) price
{
    if ( price > 0 && price < 100 )
    {
        Toy_FM* toy = [[Toy_FM alloc] init];
        
        return toy;
    }
    
    if (price >= 100)
    {
        Dress_FM* dress = [[Dress_FM alloc] init];
        
        return dress;
    }
    
    return nil;
}

@end
