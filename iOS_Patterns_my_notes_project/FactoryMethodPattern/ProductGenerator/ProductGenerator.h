//
//  ProductGenerator.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/16/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Product;

@interface ProductGenerator : NSObject

//methods
- (Product*) getProduct: (int) price;

@end
