//
//  TravellEngine.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TravellEngine : NSObject

//methods
- (void) findTransport;

- (void) orderTransport;

- (void) travel;

@end
