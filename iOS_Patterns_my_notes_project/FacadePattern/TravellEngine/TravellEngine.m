//
//  TravellEngine.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "TravellEngine.h"

@implementation TravellEngine


#pragma mark - Public methods -

- (void) findTransport
{
    NSLog(@"Okay, to travell there you will probabply need dragon!Arghhhhh");
}

- (void) orderTransport
{
    NSLog(@"Maaaam, can I order a dragon?... "
          "Yes... Yes, green one... Yes, with fire!... "
          "No, not a dragon of death... "
          "Thank you!"
          );
}

- (void) travel
{
    NSLog(@"Maaan, you are flying on dragon!");
}

@end
