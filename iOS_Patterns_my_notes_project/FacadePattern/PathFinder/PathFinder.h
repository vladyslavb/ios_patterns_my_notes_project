//
//  PathFinder.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PathFinder : NSObject

//methods
- (void) findCurrentLocation;

- (void) findLocationToTravel: (NSString*) location;

- (void) makeARoute;

@end
