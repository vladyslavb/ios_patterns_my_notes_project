//
//  PathFinder.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "PathFinder.h"

@implementation PathFinder


#pragma mark - Public methods -

- (void) findCurrentLocation
{
    NSLog(@"Finding your location. Hmmm, here you are!");
}

- (void) findLocationToTravel: (NSString*) location
{
    NSLog(@"So you wanna travell to %@", location);
}

- (void) makeARoute
{
    NSLog(@"Okay, to travell to this location we are using google maps....");
    
    //looking for path in google maps.
}

@end
