//
//  TicketPrinitingSystem.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "TicketPrinitingSystem.h"

@implementation TicketPrinitingSystem


#pragma mark - Public methods -

- (void) createTicket
{
    NSLog(@"Connecting to our ticketing system...");
}

- (void) printingTicket
{
    NSLog(@"Hmmm, ticket for travelling on the green dragon.Interesting...");
}

@end
