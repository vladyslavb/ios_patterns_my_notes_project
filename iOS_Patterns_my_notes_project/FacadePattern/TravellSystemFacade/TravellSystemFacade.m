//
//  TravellSystemFacade.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "TravellSystemFacade.h"

//clases
#import "PathFinder.h"
#import "TravellEngine.h"
#import "TicketPrinitingSystem.h"

@implementation TravellSystemFacade


#pragma mark - Public methods -

- (void) travellTo: (NSString*) location
{
    PathFinder* pathFinder = [[PathFinder alloc] init];

    TravellEngine* travellEngine = [[TravellEngine alloc] init];
    
    TicketPrinitingSystem* tpSystem = [[TicketPrinitingSystem alloc] init];
    
    [pathFinder findCurrentLocation];
    
    [pathFinder findLocationToTravel: location];
    
    [pathFinder makeARoute];
    
    [travellEngine findTransport];
    [travellEngine orderTransport];
    
    [tpSystem createTicket];
    
    [tpSystem printingTicket];
    
    [travellEngine travel];
}

@end






























