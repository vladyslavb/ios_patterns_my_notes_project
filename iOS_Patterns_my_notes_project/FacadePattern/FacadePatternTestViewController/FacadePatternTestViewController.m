//
//  FacadePatternTestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "FacadePatternTestViewController.h"

//classes
#import "TravellSystemFacade.h"

@interface FacadePatternTestViewController ()

@end

@implementation FacadePatternTestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self startTestFacadePattern];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startTestFacadePattern
{
    TravellSystemFacade* facade = [[TravellSystemFacade alloc] init];
    
    [facade travellTo: @"Kharkiv"];
}

/*
 
    LOG ------------------------>
 
         Finding your location. Hmmm, here you are!
 
         So you wanna travell to Kharkiv
 
         Okay, to travell to this location we are using google maps....
 
         Okay, to travell there you will probabply need dragon!Arghhhhh
 
         2013-02-09 17:46:28.446 FacadePattern[2410:c07]
 
         Maaaam, can I order a dragon?... Yes... Yes, green one... Yes, with fire!... No, not a dragon of death... Thank you!
 
         Connecting to our ticketing system...
 
         Hmmm, ticket for travelling on the green dragon.Interesting...
 
         Maaan, you are flying on dragon!
 
 */

@end























