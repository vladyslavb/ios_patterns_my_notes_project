//
//  FirstCommand.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BaseCommand.h"

@interface FirstCommand : BaseCommand
{
    @private NSString* _originalString;
    @private NSString* _currentString;
}

//methods
- (id) initWithArguments: (NSString*) anArgument;
- (void) printString;

@end
