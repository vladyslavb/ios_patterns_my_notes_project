//
//  FirstCommand.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "FirstCommand.h"

@implementation FirstCommand


#pragma mark - Initialization -

- (id) initWithArguments: (NSString*) anArgument
{
    self = [super init];
    
    _originalString = anArgument;
    _currentString  = anArgument;
    
    return self;
}


#pragma mark - Overriding methods -

- (void) execute
{
    _currentString = @"This is a new string";
    
    [self printString];
    
    NSLog(@"Execute command called");
}

- (void) undo
{
    _currentString = _originalString;
    
    [self printString];
    
    NSLog(@"Undo of execute command called");
}


#pragma mark - Public methods -

-(void) printString
{
    NSLog(@"Current string is equal to %@", _currentString);
}

@end
