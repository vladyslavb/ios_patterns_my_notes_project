//
//  SecondCommand.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BaseCommand.h"

@interface SecondCommand : BaseCommand
{
    @private int _originalNumber;
    @private int _currentNumber;
}

//methods
- (id) initWithArgs: (int) aNumber;
- (void) printNumber;

@end
