//
//  SecondCommand.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "SecondCommand.h"

@implementation SecondCommand


#pragma mark - Initialization -

- (id) initWithArgs: (int) aNumber
{
    self = [super init];
    
    _originalNumber = aNumber;
    
    _currentNumber = aNumber;
    
    return self;
}


#pragma mark - Overriding methods -

- (void) execute
{
    _currentNumber++;
    
    [self printNumber];
}

- (void) undo
{
    if (_currentNumber > _originalNumber)
    {
        _currentNumber--;
    }
    
    [self printNumber];
}


#pragma mark - Public methods -

- (void) printNumber
{
    NSLog(@"current number is %i", _currentNumber);
}

@end






























