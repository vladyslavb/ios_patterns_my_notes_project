//
//  CommandPatternFirstViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "CommandPatternFirstViewController.h"

//classes
#import "CommandExecutor.h"
#import "BaseCommand.h"
#import "FirstCommand.h"
#import "SecondCommand.h"

@interface CommandPatternFirstViewController ()

@end

@implementation CommandPatternFirstViewController


#pragma mark - Initialization -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self startCommandPatternTesting];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startCommandPatternTesting
{
    CommandExecutor* commandExecutor = [[CommandExecutor alloc] init];
    
    BaseCommand* firstCommand = [[FirstCommand alloc] initWithArguments: @"This is a test string"];
    
    BaseCommand* secondCommand = [[SecondCommand alloc] initWithArgs: 3];
    
    [commandExecutor addCommand: firstCommand];
    [commandExecutor addCommand: secondCommand];
    
    [commandExecutor executeCommands];
    
    [commandExecutor undoAll];
}

/*
 
 LOG -------------------->
 
         Current string is equal to This is a new string
 
         Execute command called
 
         Current number is 4
 
         Current string is equal to This is a test string
 
         Undo of execute command called
 
         Current number is 3
 
 */

@end





























