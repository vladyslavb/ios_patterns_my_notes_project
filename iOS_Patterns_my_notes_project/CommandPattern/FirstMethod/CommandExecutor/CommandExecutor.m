//
//  CommandExecutor.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "CommandExecutor.h"

//classes
#import "BaseCommand.h"

@implementation CommandExecutor


#pragma mark - Initialization -

- (id) init
{
    self = [super init];
    
    _arrayOfCommands = [[NSMutableArray alloc] init];
    
    return self;
}


#pragma mark - Public methods -

-(void) addCommand: (BaseCommand*) aCommand
{
    //id<CommandProtocol> item = aCommand;
    
    [_arrayOfCommands addObject: aCommand];
}

- (void) executeCommands
{
    for (BaseCommand* command in _arrayOfCommands)
    {
        [command execute];
    }
}

- (void) undoAll
{
    for (BaseCommand* command in _arrayOfCommands)
    {
        [command undo];
    }
}

@end
