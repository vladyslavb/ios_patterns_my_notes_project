//
//  CommandExecutor.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseCommand;

@interface CommandExecutor : NSObject
{
    @private NSMutableArray* _arrayOfCommands;
}

//methods

-(void) addCommand: (BaseCommand*) aCommand;
-(void) executeCommands;
-(void) undoAll;


@end
