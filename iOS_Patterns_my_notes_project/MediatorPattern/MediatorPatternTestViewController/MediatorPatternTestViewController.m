//
//  MediatorPatternTestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MediatorPatternTestViewController.h"

//classes
#import "CentrallProcessor.h"
#import "Thermometer.h"
#import "ConditioningSystem.h"

@interface MediatorPatternTestViewController ()

@end

@implementation MediatorPatternTestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self startTestMediatorPattern];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startTestMediatorPattern
{
    CentrallProcessor* proccessor = [[CentrallProcessor alloc] init];
    
    Thermometer* therm = [[Thermometer alloc] initWithCore: proccessor];
    
    ConditioningSystem* condSystem =
        [[ConditioningSystem alloc] initWithCore: proccessor];
    
    proccessor._condSystem = condSystem;
    
    proccessor._thermometer = therm;
    
    [therm temperatureChanged: 45];
}

/*
 
    LOG ------------------>
 
         Value changed! We need to do smth!
 
         Oh, the change is temperature
 
         Conditioning...
 
 */


@end


































