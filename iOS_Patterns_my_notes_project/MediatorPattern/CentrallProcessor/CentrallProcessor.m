//
//  CentrallProcessor.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "CentrallProcessor.h"

//classes
#import "Thermometer.h"
#import "ConditioningSystem.h"

@implementation CentrallProcessor


#pragma mark - Public methods -

- (void) valueChanged: (SmartHousePart*) aPart
{
    NSLog(@"Value changed! We need to do smth!");
    
    //detecting that changes are done by thermometer
    if ( [aPart isKindOfClass: [Thermometer class]])
    {
        NSLog(@"Oh, the change is temperature");
        
        [[self _condSystem] startCondition];
    }
}

@end
