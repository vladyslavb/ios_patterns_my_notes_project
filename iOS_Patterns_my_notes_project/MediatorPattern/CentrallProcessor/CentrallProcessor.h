//
//  CentrallProcessor.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//classes
@class Thermometer;
@class ConditioningSystem;
@class SmartHousePart;

@interface CentrallProcessor : NSObject

//properties
@property (weak, nonatomic) Thermometer*        _thermometer;
@property (weak, nonatomic) ConditioningSystem* _condSystem;

//methods
- (void) valueChanged: (SmartHousePart*) aPart;

@end
