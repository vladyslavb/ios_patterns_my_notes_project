//
//  SmartHousePart.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//classes
@class CentrallProcessor;

@interface SmartHousePart : NSObject
{
    @private CentrallProcessor* _processor;
}

//methods
- (id) initWithCore: (CentrallProcessor*) processor;

- (void) numbersChanged;

@end
