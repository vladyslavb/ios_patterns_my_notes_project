//
//  SmartHousePart.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "SmartHousePart.h"

//classes
#import "CentrallProcessor.h"

@implementation SmartHousePart


#pragma mark - Public methods -

- (id) initWithCore: (CentrallProcessor*) processor
{
    if (self = [super init])
    {
       _processor = processor;
    }
    
    return self;
}

- (void) numbersChanged
{
    [_processor valueChanged: self];
}


@end
