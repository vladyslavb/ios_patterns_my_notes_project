//
//  ConditioningSystem.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ConditioningSystem.h"

@implementation ConditioningSystem


#pragma mark - Public methods -

- (void) startCondition
{
    NSLog(@"Conditioning...");
}

@end
