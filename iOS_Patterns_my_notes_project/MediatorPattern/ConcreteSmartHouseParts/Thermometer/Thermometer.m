//
//  Thermometer.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Thermometer.h"

@implementation Thermometer


#pragma mark - Public methods -

- (void) temperatureChanged: (int) temperature
{
    self.temperature = temperature;
    
    [self numbersChanged];
}

@end
