//
//  CompositeObjectProtocol.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CompositeObjectProtocol <NSObject>

//required methods
- (NSString*) getData;

- (void) addComponent: (id<CompositeObjectProtocol>) aComponent;

@end
