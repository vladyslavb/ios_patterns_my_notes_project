//
//  CompositePatternViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "CompositePatternViewController.h"

//classes
#import "Container.h"
#import "LeafObject.h"

@interface CompositePatternViewController ()

@end

@implementation CompositePatternViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self startCompositePatternTesting];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startCompositePatternTesting
{
    Container* rootContainer = [[Container alloc] init];
    
    LeafObject* object = [[LeafObject alloc] init];
    
    object.leafValue = @"level1 value";
    
    [rootContainer addComponent: object];
    
    Container* firstLevelContainer1 = [[Container alloc] init];
    
    LeafObject* object2 = [[LeafObject alloc] init];
    
    object2.leafValue = @"level2 value";
    
    [firstLevelContainer1 addComponent: object2];
    
    [rootContainer addComponent: firstLevelContainer1];
    
    Container* firstLevelContainer2 = [[Container alloc] init];
    
    LeafObject* object3 = [[LeafObject alloc] init];
    
    object3.leafValue = @"level2 value 2";
    
    [firstLevelContainer2 addComponent: object3];
    
    [rootContainer addComponent: firstLevelContainer2];
    
    NSLog(@"%@", rootContainer.getData);
}

/*
 
    LOG ----------------------->
 
 <ContainerValues>
        <level1 value/>
 
        <ContainerValues>
                <level2 value/>
        </ContainerValues>
 
        <ContainerValues>
                <level2 value 2/>
        </ContainerValues>
 </ContainerValues>
 
 */


@end
































