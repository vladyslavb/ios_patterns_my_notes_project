//
//  Container.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Container.h"

@implementation Container


#pragma mark - Initialization -

- (NSMutableArray*) components
{
    if (_components == nil)
    {
        _components = [[NSMutableArray alloc] init];
    }
    
    return _components;
}


#pragma mark - CompositeObjectProtocol methods -

- (void) addComponent: (id<CompositeObjectProtocol>) aComponent
{
    [self.components addObject: aComponent];
}

- (NSString*) getData
{
    NSMutableString* valueToReturn = [[NSMutableString alloc] init];
    
    [valueToReturn appendString: @"<ContainerValues>"];
    
    for ( id<CompositeObjectProtocol> object in _components)
    {
        [valueToReturn appendString: [object getData]];
    }
    
    [valueToReturn appendString: @"</ContainerValues>"];
    
    return valueToReturn; }

@end





























