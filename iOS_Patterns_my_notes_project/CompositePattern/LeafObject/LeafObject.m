//
//  LeafObject.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "LeafObject.h"

@implementation LeafObject


#pragma mark - CompositeObjectProtocol methods -

- (NSString*) getData
{
    return [[NSString alloc] initWithFormat: @"<%@/>", self.leafValue ];
}

- (void) addComponent: (id) aComponent
{
    NSLog(@"Can't add component. Sorry, man");
}

@end
