//
//  GoodItemsIterator.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "GoodItemsEnumerator.h"

//classes
#import "ItemInShop.h"

@implementation GoodItemsEnumerator


#pragma mark - Initialization -

- (id) initWithItems: (NSMutableArray*) anItems
{
    self = [super init];
    
    itemsArray = [[NSMutableArray alloc] init];
    
    for ( ItemInShop* item in anItems)
    {
        if (!item.isBroken)
        {
            [itemsArray addObject: item];
        }
    }
    
    internalEnumerator = [itemsArray objectEnumerator];
    
    return self;
}


#pragma mark - Overriding methods -

- (NSArray*) allObjects
{
    return itemsArray;
}

- (id) nextObject
{
    return [internalEnumerator nextObject];
}

@end
