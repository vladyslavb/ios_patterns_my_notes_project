//
//  ShopWarehouse.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ShopWarehouse.h"

//classes
#import "BadItemsEnumerator.h"
#import "GoodItemsEnumerator.h"

@implementation ShopWarehouse


#pragma mark - Initialization -

- (id) init
{
    if(self = [super init])
    {
      goods = [[NSMutableArray alloc] init];
    }

    return self;
}


#pragma mark - Public methods -

- (void) addItem: (ItemInShop*) anItem
{
    [goods addObject: anItem];
}

- (NSEnumerator*) getBrokenItemsEnumerator
{
    badItemsEnumerator = [[BadItemsEnumerator alloc] initWithItems: goods];
    
    return badItemsEnumerator;
}

- (NSEnumerator*) getGoodItemsEnumerator
{
    goodItemsEnumerator = [[GoodItemsEnumerator alloc] initWithItems: goods];
    
    return goodItemsEnumerator;
}


@end
