//
//  ShopWarehouse.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//classes
@class ItemInShop;

#import "GoodItemsEnumerator.h"
#import "BadItemsEnumerator.h"

@interface ShopWarehouse : NSObject
{
    @private NSMutableArray*      goods;
    @private GoodItemsEnumerator* goodItemsEnumerator;
    @private BadItemsEnumerator*  badItemsEnumerator;
}

//methods
- (void) addItem: (ItemInShop*) anItem;

- (NSEnumerator*) getBrokenItemsEnumerator;

- (NSEnumerator*) getGoodItemsEnumerator;

@end
