//
//  IteratorPatternViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "IteratorPatternViewController.h"

//classes
#import "ShopWarehouse.h"
#import "ItemInShop.h"
#import "BadItemsEnumerator.h"
#import "GoodItemsEnumerator.h"

@interface IteratorPatternViewController ()

@end

@implementation IteratorPatternViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self startIteratorPatternTesting];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Initialization -

- (void) startIteratorPatternTesting
{
    // Creating test data
    ShopWarehouse* shopWarehouse = [[ShopWarehouse alloc] init];
    
    [shopWarehouse addItem: [[ItemInShop alloc] initWithArgs: @"Item1"
                                                  andQaulity: NO]];
    
    [shopWarehouse addItem: [[ItemInShop alloc] initWithArgs: @"Item2"
                                                  andQaulity: NO]];
    
    [shopWarehouse addItem: [[ItemInShop alloc] initWithArgs: @"Item3"
                                                  andQaulity: YES]];
    
    [shopWarehouse addItem: [[ItemInShop alloc] initWithArgs: @"Item4"
                                                  andQaulity: YES]];
    
    [shopWarehouse addItem: [[ItemInShop alloc] initWithArgs: @"Item5"
                                                  andQaulity: NO]];
    
    //Testing
    
    GoodItemsEnumerator* goodIterator = (GoodItemsEnumerator*) [shopWarehouse getGoodItemsEnumerator];
    
    BadItemsEnumerator* badIterator = (BadItemsEnumerator*) [shopWarehouse getBrokenItemsEnumerator];
    
    ItemInShop* element;
    
    while (element = [goodIterator nextObject])
    {
        NSLog(@"Good Item = %@", element.name);
    }
    
    while (element = [badIterator nextObject])
    {
        NSLog(@"Bad Item = %@", element.name);
    }
}

/*
 
 LOG --------------------------------->
 
 Good Item = Item1
 Good Item = Item2
 Good Item = Item5
 Bad  Item = Item3
 Bad  Item = Item4
 
 */


@end

































