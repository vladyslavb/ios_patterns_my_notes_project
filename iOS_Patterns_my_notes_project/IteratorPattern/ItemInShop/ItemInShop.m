//
//  ItemInShop.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ItemInShop.h"

@implementation ItemInShop


#pragma mark - Initialization -

- (id) initWithArgs: (NSString*) aName
         andQaulity: (BOOL)      isBroken
{
    if (self = [super init])
    {
        self.name     = aName;
        self.isBroken = isBroken;
    }
    
    return self;
}

@end
