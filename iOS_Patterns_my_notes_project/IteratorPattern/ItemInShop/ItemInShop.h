//
//  ItemInShop.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItemInShop : NSObject

//properties
@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) BOOL      isBroken;

//methods
- (id) initWithArgs: (NSString*) aName
         andQaulity: (BOOL)      isBroken;

@end
