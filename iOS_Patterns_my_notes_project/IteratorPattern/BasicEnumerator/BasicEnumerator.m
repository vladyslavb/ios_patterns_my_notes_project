//
//  BasicEnumerator.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BasicEnumerator.h"

@implementation BasicEnumerator


#pragma mark - Overriding methods -

- (id) initWithItems: (NSMutableArray*) anItems
{
    return nil;
}

- (NSArray*) allObjects
{
    return nil;
}

- (id) nextObject
{
    return nil;
}

@end
