//
//  RavenAdapter.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "RavenAdapter.h"

@implementation RavenAdapter

//TODO: for unused (for older versions)
@synthesize raven = _raven;


#pragma mark - Initialization -

- (id) initWithRaven: (Raven*) adaptee
{
    self = [super self];
    
    _raven = adaptee;
    
    return self;
}


#pragma mark - Overrided methods -

- (void) sing
{
    [_raven voice ];
}

- (void) fly
{
    [_raven flySearchAndDestroy];
}


@end
