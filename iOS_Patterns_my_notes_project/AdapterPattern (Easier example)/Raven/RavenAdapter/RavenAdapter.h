//
//  RavenAdapter.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//protocols
#import "BirdProtocol.h"

//clases
#import "Raven.h"

@interface RavenAdapter : NSObject <BirdProtocol>
{
    @private Raven* _raven;
}

//properties
@property (strong, nonatomic) Raven* raven;

//methods
- (id) initWithRaven: (Raven*) adaptee;

@end
