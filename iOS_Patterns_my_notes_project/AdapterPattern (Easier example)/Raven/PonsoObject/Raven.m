//
//  Raven.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Raven.h"

@implementation Raven


#pragma mark - Public methods -

- (void) flySearchAndDestroy
{
    NSLog(@"I am flying and seak for killing!");
}

- (void) voice
{
    NSLog(@"Kaaaar-kaaaaar-kaaaaaaar!");
}

@end
