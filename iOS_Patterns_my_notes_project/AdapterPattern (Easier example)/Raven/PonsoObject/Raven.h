//
//  Raven.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Raven : NSObject

//methods
- (void) voice;
- (void) flySearchAndDestroy;

@end
