//
//  BirdProtocol.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BirdProtocol <NSObject>

//required methods
- (void) sing;
- (void) fly;

@end
