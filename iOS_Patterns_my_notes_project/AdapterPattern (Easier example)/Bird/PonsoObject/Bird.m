//
//  Bird.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Bird.h"

@implementation Bird


#pragma mark - Bird Protocol methods -

- (void) sing
{
    NSLog(@"Tew-tew-tew");
}

- (void) fly
{
    NSLog(@"OMG! I am flying!");
}

@end
