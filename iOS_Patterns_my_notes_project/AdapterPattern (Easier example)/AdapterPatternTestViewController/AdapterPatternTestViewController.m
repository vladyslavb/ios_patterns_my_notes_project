//
//  AdapterPatternTestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AdapterPatternTestViewController.h"

//protocols
#import "BirdProtocol.h"

//classes
#import "Bird.h"
#import "Raven.h"
#import "RavenAdapter.h"

@interface AdapterPatternTestViewController ()

@end

@implementation AdapterPatternTestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self makeTheBirdTest];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) makeTheBirdTest
{
    Bird * simpleBird =  [[Bird  alloc] init];
    Raven* simpleRaven = [[Raven alloc] init];
    
    RavenAdapter* ravenAdapter = [[RavenAdapter alloc] initWithRaven: simpleRaven];
    
    [self makeTheBirdFunctional: simpleBird];
    [self makeTheBirdFunctional: ravenAdapter];
}

- (void) makeTheBirdFunctional: (id<BirdProtocol>) aBird
{
    [aBird fly];
    [aBird sing];
}

/*
 LOG ----------------->
 
    OMG! I am flying!
    Tew-tew-tew
    I am flying and seak for killing!
    Kaaaar-kaaaaar-kaaaaaaar!
 
 */


@end

























