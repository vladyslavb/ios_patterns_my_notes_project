//
//  StrategyPatternTestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "StrategyPatternTestViewController.h"

//classes
#import "Player.h"
#import "AttackStrategy.h"
#import "DefenceStrategy.h"

@interface StrategyPatternTestViewController ()

@end

@implementation StrategyPatternTestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self startStrategyPatternTesting];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startStrategyPatternTesting
{
    Player* player = [[Player alloc] init];
    
    AttackStrategy*  actionStrategy  = [[AttackStrategy  alloc] init];
    DefenceStrategy* defenceStrategy = [[DefenceStrategy alloc] init];
    
    [player changeStrategy: actionStrategy];
    
    [player makeAction];
    
    [player changeStrategy: defenceStrategy];
    
    [player makeAction];
}

/*
 
 LOG -------------------->
 
 Character 1: Attack all enemies!
 Character 2: Attack all enemies!
 Character 3: Attack all enemies!
 
 Character 1: Attack all enemies!
 Character 2: Healing Character 1!
 Character 3: Protecting Character 2!
 
 */


@end





































