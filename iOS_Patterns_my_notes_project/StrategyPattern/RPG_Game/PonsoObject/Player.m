//
//  Player.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Player.h"

@implementation Player


#pragma mark - Public methods -

- (void) makeAction
{
    [self._strategy actionCharacter1];
    [self._strategy actionCharacter2];
    [self._strategy actionCharacter3];
}

- (void) changeStrategy: (BasicStrategy*) strategy
{
    self._strategy = strategy;
}

@end
