//
//  Player.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//classes
#import "BasicStrategy.h"

@interface Player : NSObject

//properties
@property (strong, nonatomic) BasicStrategy* _strategy;

//methods
-(void) makeAction;
-(void) changeStrategy: (BasicStrategy*) strategy;


@end
