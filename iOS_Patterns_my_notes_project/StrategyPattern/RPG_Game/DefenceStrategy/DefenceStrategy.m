//
//  DefenceStrategy.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "DefenceStrategy.h"

@implementation DefenceStrategy


#pragma mark - Overriding methods -

- (void) actionCharacter1
{
    NSLog(@"Character 1: Attack all enemies!");
}

- (void) actionCharacter2
{
    NSLog(@"Character 2: Healing Character 1!");
}

- (void) actionCharacter3
{
    NSLog(@"Character 3: Protecting Character 2!");
}

@end
