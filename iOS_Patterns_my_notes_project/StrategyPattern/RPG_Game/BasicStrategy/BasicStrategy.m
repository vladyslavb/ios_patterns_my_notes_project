//
//  BasicStrategy.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BasicStrategy.h"

@implementation BasicStrategy


#pragma mark - Overriding methods -

- (void) actionCharacter1
{
    
}

- (void) actionCharacter2
{
    
}

- (void) actionCharacter3
{
    
}

@end
