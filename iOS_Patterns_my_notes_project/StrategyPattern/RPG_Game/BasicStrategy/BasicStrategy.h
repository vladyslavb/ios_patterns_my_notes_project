//
//  BasicStrategy.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BasicStrategy : NSObject

//methods
-(void) actionCharacter1;
-(void) actionCharacter2;
-(void) actionCharacter3;

@end
