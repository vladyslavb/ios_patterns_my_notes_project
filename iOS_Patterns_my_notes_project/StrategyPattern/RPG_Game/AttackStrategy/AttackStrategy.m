//
//  AttackStrategy.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "AttackStrategy.h"

@implementation AttackStrategy


#pragma mark - Overriding methods -

- (void) actionCharacter1
{
    NSLog(@"Character 1: Attack all enemies!");
}

- (void) actionCharacter2
{
    NSLog(@"Character 2: Attack all enemies!");
}

- (void) actionCharacter3
{
    NSLog(@"Character 3: Attack all enemies!");
}

@end
