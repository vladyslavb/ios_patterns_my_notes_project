//
//  KVOObserver.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "KVOObserver.h"

@implementation KVOObserver


#pragma mark - KVO methods -

- (void) observeValueForKeyPath: (NSString*)                             keyPath
                       ofObject: (id)                                    object
                         change: (NSDictionary<NSKeyValueChangeKey,id>*) change
                        context: (void*)                                 context
{
    NSLog(@"KVO: Value changed;");
}

@end
