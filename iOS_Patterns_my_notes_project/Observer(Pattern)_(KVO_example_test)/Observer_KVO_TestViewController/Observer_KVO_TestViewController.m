//
//  Observer_KVO_TestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Observer_KVO_TestViewController.h"

//classes
#import "KVOSubject.h"
#import "KVOObserver.h"

@interface Observer_KVO_TestViewController ()

@end

@implementation Observer_KVO_TestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self startObserverPatternKVO_Test];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startObserverPatternKVO_Test
{
    KVOSubject* kvoSubj = [[KVOSubject alloc] init];
    
    KVOObserver* kvoObserver = [[KVOObserver alloc] init];
    
    [kvoSubj addObserver: kvoObserver
              forKeyPath: @"changeableProperty"
                 options: NSKeyValueObservingOptionNew
                 context: nil];
    
    [kvoSubj setValue: @"new value"
               forKey: @"changeableProperty"];
    
    /*
     
     because kvoSubj will be deallocated after this
     functions ends we need to remove observer information.
     
     [kvoSubj removeObserver: kvoObserver
                  forKeyPath: @"changeableProperty"];
     
     */
    
    
    
    /*
     
        LOG ------------------------>
     
             KVO: Value changed;
             
             KVO: Value changed;
     
     */
    
}


@end





































