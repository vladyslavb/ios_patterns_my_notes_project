//
//  SingletonObject.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingletonObject : NSObject

//properties
@property (weak,nonatomic) NSString* tempProperty;

//public methods
+ (SingletonObject*) singleton;

@end
