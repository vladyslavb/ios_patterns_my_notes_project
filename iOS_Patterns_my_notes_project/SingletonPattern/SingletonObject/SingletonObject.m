//
//  SingletonObject.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "SingletonObject.h"

@implementation SingletonObject


#pragma mark - Initialization -

+ (SingletonObject*) singleton
{
    static SingletonObject* singletonObject = nil;
    
    static dispatch_once_t onceToken; dispatch_once(&onceToken, ^{
        singletonObject = [[self alloc] init];
    });
    
    return singletonObject;
}

@end
