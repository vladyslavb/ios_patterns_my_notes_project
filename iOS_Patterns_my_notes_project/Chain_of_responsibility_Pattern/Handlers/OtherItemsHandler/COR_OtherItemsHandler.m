//
//  COR_OtherItemsHandler.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "COR_OtherItemsHandler.h"

//classes
#import "COR_BasicItem.h"

@implementation COR_OtherItemsHandler


#pragma mark - Overriding methods -

- (void) handleItem: (COR_BasicItem*) item
{
    NSLog(@"Found undefined item. Destroying");
}

@end
