//
//  COR_ElectronicsHandler.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "COR_ElectronicsHandler.h"

//classes
#import "COR_BasicItem.h"
#import "Electronics.h"

@implementation COR_ElectronicsHandler


#pragma mark - Overriding methods -

-(void) handleItem: (COR_BasicItem*) item
{
    if ([item isKindOfClass: [Electronics class]])
    {
        NSLog(@"Electronics found. Handling");
    }
    else
    {
        NSLog(@"Electronics not found. Handling using next handler");
        
        [self.nextHandler handleItem: item];
    }
}

@end
