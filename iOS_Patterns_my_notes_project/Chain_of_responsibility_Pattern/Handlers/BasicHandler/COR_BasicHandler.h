//
//  COR_BasicHandler.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@class COR_BasicItem;

@interface COR_BasicHandler : NSObject
{
    @private COR_BasicHandler* _nextHandler;
}

//properties
@property (strong, nonatomic) COR_BasicHandler* nextHandler;

//methods
- (void) handleItem: (COR_BasicItem*) item;

@end
