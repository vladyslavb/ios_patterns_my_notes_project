//
//  COR_ToysHandler.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "COR_ToysHandler.h"

//classes
#import "Toy.h"
#import "COR_BasicItem.h"

@implementation COR_ToysHandler


#pragma mark - Overriding methods -

-(void) handleItem: (COR_BasicItem*) item
{
    if ([item isKindOfClass: [Toy class]])
    {
        NSLog(@"Toy found. Handling");
    }
    else
    {
        NSLog(@"Toy not found. Handling using next handler");
        
        [self.nextHandler handleItem: item];
    }
}

@end
