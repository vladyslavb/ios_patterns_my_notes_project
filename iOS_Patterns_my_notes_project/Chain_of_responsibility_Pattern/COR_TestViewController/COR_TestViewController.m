//
//  COR_TestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "COR_TestViewController.h"

//classes
#import "COR_BasicItem.h"
#import "Toy.h"
#import "Electronics.h"
#import "COR_Trash.h"

#import "COR_BasicHandler.h"
#import "COR_ToysHandler.h"
#import "COR_ElectronicsHandler.h"
#import "COR_OtherItemsHandler.h"

@interface COR_TestViewController ()

@end

@implementation COR_TestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self startCOR_PatternTesting];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startCOR_PatternTesting
{
    COR_BasicHandler* toysHandler = [[COR_ToysHandler alloc] init];
    
    COR_BasicHandler* electronicsHandler = [[COR_ElectronicsHandler alloc] init];
    
    COR_BasicHandler* otherItemHandler = [[COR_OtherItemsHandler alloc]init];
    
    electronicsHandler.nextHandler = otherItemHandler;
    
    toysHandler.nextHandler = electronicsHandler;
    
    COR_BasicItem* toy = [[Toy alloc] init];
    
    COR_BasicItem* electronic = [[Electronics alloc] init];
    
    COR_BasicItem* trash = [[COR_Trash alloc] init];
    
    [toysHandler handleItem: toy];
    [toysHandler handleItem: electronic];
    [toysHandler handleItem: trash];
}

/*
 
    LOG ------------------>
 
             Toy found. Handling
 
             Toy not found. Handling using next handler
 
             Electronics found. Handling
 
             Toy not found. Handling using next handler
 
             Electronics not found. Handling using next handler
 
             Found undefined item. Destroying
 
 */

@end


































