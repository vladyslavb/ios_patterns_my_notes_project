//
//  ExpensiveHeadphones.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ExpensiveHeadphones.h"

@implementation ExpensiveHeadphones


#pragma mark - Overriding methods -

- (void) playSimpleSound
{
    NSLog(@"(playSimpleSound): Beep-Beep-Beep Taram - Rararam");
}

- (void) playBassSound
{
    NSLog(@"(playBassSound): Bam-Bam-Bam");
}

@end
