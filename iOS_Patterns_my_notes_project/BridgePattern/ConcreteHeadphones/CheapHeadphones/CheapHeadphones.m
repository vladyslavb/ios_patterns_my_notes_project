//
//  CheapHeadphones.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "CheapHeadphones.h"

@implementation CheapHeadphones


#pragma mark - Overriding methods -

- (void) playSimpleSound
{
    NSLog(@"(playSimpleSound): beep - beep - bhhhrhrhrep");
}

- (void) playBassSound
{
    NSLog(@"(playBassSound): puf - puf - pufhrrr");
}

@end
