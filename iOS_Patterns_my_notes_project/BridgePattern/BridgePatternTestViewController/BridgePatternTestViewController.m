//
//  BridgePatternTestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BridgePatternTestViewController.h"

//classes
#import "MusicPlayer.h"
#import "CheapHeadphones.h"
#import "ExpensiveHeadphones.h"

@interface BridgePatternTestViewController ()

@end

@implementation BridgePatternTestViewController


#pragma mark - Life cycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - Memory managment -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) startBridgePatternTest
{
    MusicPlayer* musicPlayer = [[MusicPlayer alloc] init];
    
    CheapHeadphones* cheapHeadphones = [[CheapHeadphones alloc] init];
    
    ExpensiveHeadphones* expensiveHeadphones = [[ExpensiveHeadphones alloc] init];
    
    musicPlayer.headPhones = cheapHeadphones;
    
    [musicPlayer playMusic];
    
    musicPlayer.headPhones = expensiveHeadphones;
    
    [musicPlayer playMusic];
}

/*
 
 LOG ------------------------>
 
     puf – puf – pufhrrr
     puf – puf – pufhrrr
 
     beep – beep – bhhhrhrhrep
     beep – beep – bhhhrhrhrep
 
     Bam-Bam-Bam
     Bam-Bam-Bam
 
     Beep-Beep-Beep Taram – Rararam
     Beep-Beep-Beep Taram – Rararam
 
 */


@end































