//
//  MusicPlayer.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//clases
#import "BaseHeadphones.h"

@interface MusicPlayer : NSObject

//properties
@property (strong, nonatomic) BaseHeadphones* headPhones;

//methods
- (void) playMusic;

@end
