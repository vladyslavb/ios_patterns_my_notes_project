//
//  MusicPlayer.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/22/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MusicPlayer.h"

@implementation MusicPlayer


#pragma mark - Public methods -

- (void) playMusic
{
    [self.headPhones playBassSound];
    [self.headPhones playBassSound];
    
    [self.headPhones playSimpleSound];
    [self.headPhones playSimpleSound];
}

@end
