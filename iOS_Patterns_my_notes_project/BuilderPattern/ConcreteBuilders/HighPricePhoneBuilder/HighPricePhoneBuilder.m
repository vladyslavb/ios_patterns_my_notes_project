//
//  HighPricePhoneBuilder.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "HighPricePhoneBuilder.h"

@implementation HighPricePhoneBuilder


#pragma mark - Overrided methods -

- (void) setOSVersion
{
    self.phone.osVersion = @"Android 4.1";
}

- (void) setName
{
    self.phone.name = @"High price phone!";
}

- (void) setCPUCodeName
{
    self.phone.cpuCodeName = @"Some shitty but expensive CPU";
}

- (void) setRAMSize
{
    self.phone.RAMsize = [[NSNumber alloc] initWithInt: 1024];
}

- (void) setOSVersionCode
{
    self.phone.osVersionCode = [[NSNumber alloc] initWithFloat: 4.1f];
}

- (void) setLauncher
{
    self.phone.launcher = @"Samsung Launcher";
}




@end
