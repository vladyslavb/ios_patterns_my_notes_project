//
//  LowPricePhoneBuilder.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "LowPricePhoneBuilder.h"

//clases
#import "BPAndroidPhoneBuilder.h"

@implementation LowPricePhoneBuilder


#pragma mark - Overrided methods -

- (void) setOSVersion
{
    self.phone.osVersion = @"Android 2.3";
}

- (void) setName
{
    self.phone.name = @"Low price phone!";
}

- (void) setCPUCodeName
{
    self.phone.cpuCodeName = @"Some shitty CPU";
}

- (void) setRAMSize
{
    self.phone.RAMsize = [[NSNumber alloc] initWithInt: 256];
}

- (void) setOSVersionCode
{
    self.phone.osVersionCode = [[NSNumber alloc] initWithFloat: 3.0f];
}

- (void) setLauncher
{
    self.phone.launcher = @"Hia Tsung!";
}

@end
