//
//  FactorySalesMan.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "FactorySalesMan.h"

@implementation FactorySalesMan

- (void) setBulider: (BPAndroidPhoneBuilder*) aBuilder
{
    self._builder = aBuilder;
}

- (AndroidPhone*) getPhone
{
    return self._builder.getPhone;
}

- (void) constructPhone
{
    [self._builder setName];
    [self._builder setOSVersion];
    [self._builder setCPUCodeName];
    [self._builder setOSVersionCode];
    [self._builder setRAMSize];
    [self._builder setLauncher];
}

@end
