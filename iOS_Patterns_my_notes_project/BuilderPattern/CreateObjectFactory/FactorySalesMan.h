//
//  FactorySalesMan.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//clases
#import "AndroidPhone.h"
#import "BPAndroidPhoneBuilder.h"

@interface FactorySalesMan : NSObject

//properties
@property (nonatomic, strong) BPAndroidPhoneBuilder *_builder;

//methods
- (void) constructPhone;
- (AndroidPhone*) getPhone;
- (void) setBulider: (BPAndroidPhoneBuilder*) aBuilder;

@end
