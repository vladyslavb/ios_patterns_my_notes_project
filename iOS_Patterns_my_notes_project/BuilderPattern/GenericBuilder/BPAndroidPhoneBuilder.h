//
//  BPAndroidPhoneBuilder.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

//classes
#import "AndroidPhone.h"

@interface BPAndroidPhoneBuilder : NSObject

//properties
@property (nonatomic, strong) AndroidPhone* phone;

//methods
- (AndroidPhone*) getPhone;

//abstract methods
- (void) setName;
- (void) setOSVersion;
- (void) setCPUCodeName;
- (void) setRAMSize;
- (void) setOSVersionCode;
- (void) setLauncher;

@end
