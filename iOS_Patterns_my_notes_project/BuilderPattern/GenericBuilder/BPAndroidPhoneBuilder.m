//
//  BPAndroidPhoneBuilder.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BPAndroidPhoneBuilder.h"

@implementation BPAndroidPhoneBuilder


#pragma mark - Initialization -

- (id) init
{
    self = [super init];
    
    self.phone = [[AndroidPhone alloc] init];
    
    return self;
}


#pragma mark - Public methods -

- (AndroidPhone*) getPhone
{
    return self.phone;
}


#pragma mark - Public abstact methods -

- (void) setName
{
    
}

- (void) setOSVersion
{
    
}

- (void) setCPUCodeName
{
    
}

- (void) setRAMSize
{
    
}

- (void) setOSVersionCode
{
    
}

- (void) setLauncher
{
    
}

@end
