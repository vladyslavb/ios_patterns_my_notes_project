//
//  AndroidPhone.h
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AndroidPhone : NSObject

//property
@property (weak, nonatomic)   NSString* name;
@property (weak, nonatomic)   NSString* launcher;
@property (weak, nonatomic)   NSString* osVersion;
@property (weak, nonatomic)   NSString* cpuCodeName;
@property (strong, nonatomic) NSNumber* RAMsize;
@property (strong, nonatomic) NSNumber* osVersionCode;

@end
