//
//  BuilderTestViewController.m
//  iOS_Patterns_my_notes_project
//
//  Created by Vladyslav Bedro on 8/21/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BuilderTestViewController.h"

//clases
#import "LowPricePhoneBuilder.h"
#import "HighPricePhoneBuilder.h"

#import "FactorySalesMan.h"

@interface BuilderTestViewController ()

@end

@implementation BuilderTestViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self makeTestBuilderPattern];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) makeTestBuilderPattern
{
    LowPricePhoneBuilder* _cheapPhoneBuilder = [[LowPricePhoneBuilder alloc] init];
    
    HighPricePhoneBuilder* _expensivePhoneBuilder = [[HighPricePhoneBuilder alloc] init];
    
    FactorySalesMan* _salesMan = [[FactorySalesMan alloc] init];
    
    [_salesMan setBulider: _cheapPhoneBuilder];
    [_salesMan constructPhone];
    
    AndroidPhone* _phone = [_salesMan getPhone];
    
    NSLog(@"Phone Name = %@, osVersion = %@, cpu code name = %@, ram size = %@, osversion code = %@, launcher = %@",
                _phone.name,
                _phone.osVersion,
                _phone.cpuCodeName,
                _phone.RAMsize,
                _phone.osVersionCode,
                _phone.launcher);
    
    [_salesMan setBulider:_expensivePhoneBuilder];
    [_salesMan constructPhone];
    
    _phone = [_salesMan getPhone];
    
    NSLog(@"Phone Name = %@, osVersion = %@, cpu code name = %@, ram size = %@, os version code = %@, launcher = %@",
                _phone.name,
                _phone.osVersion,
                _phone.cpuCodeName,
                _phone.RAMsize,
                _phone.osVersionCode,
                _phone.launcher);
}

/*
 
 LOG -------------------------->
 
 Phone Name = Low price phone!, osVersion = Android 2.3, cpu code name = Some shitty CPU, ram size = 256, os version code = 3, launcher = Hia Tsung!
 
 Phone Name = High price phone!, osVersion = Android 4.1, cpu code name = Some shitty but expensive CPU, ram size = 1024, os version code = 4.1, launcher = Samsung Launcher
 
 */

















@end
